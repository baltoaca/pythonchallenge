#!/usr/bin/python2
import sys
import string

secret = open('in','r').read()
secret = filter(lambda x:x in string.letters, secret)

def extract(list):
    if  len(list) == 9 and list[0].islower() and list[8].islower() and \
        list[1:4].isupper() and list[5:8].isupper() and list[4].islower():
        print list[4],
        #print "DEBUG", list

def decode(string):
    l = string[:9]
    start = 0
    end = 9
    while l:
        start = start + 1
        end = end + 1
        l = string[start:end]
        extract(l)


decode(secret)
